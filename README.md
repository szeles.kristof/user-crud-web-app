# User CRUD Web App

A Java Web Application that provides the C and R in the CRUD operations on a User entity.

To build and run the application run these two commands in the root of the project:
* gradle wrapper
* ./gradlew run

After that, you can access the webapp with your web browser at [http://localhost:8080/](http://localhost:8080/).
