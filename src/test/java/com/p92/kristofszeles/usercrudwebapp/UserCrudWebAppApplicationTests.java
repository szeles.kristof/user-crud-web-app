package com.p92.kristofszeles.usercrudwebapp;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserCrudWebAppApplicationTests {

    @Autowired
    private UserRepository repository;

    @Test
    public void test1() { // no users
        Assert.assertEquals(repository.findAll().toString(), "[]");
    }

    @Test
    public void test2() { // add one user with one valid and one invalid emails
        List<String> emails = new ArrayList<>();
        emails.add("example@example.com");
        emails.add("foo");
        repository.save(new User("user1", LocalDate.parse("1980-01-01"), emails));
        Assert.assertEquals(repository.findAll().toString(), "[User{id=1, name=user1, birthday=1980-01-01, emails=[example@example.com]}]");
    }
}
