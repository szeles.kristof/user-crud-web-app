package com.p92.kristofszeles.usercrudwebapp;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.ws.rs.FormParam;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author szelesk
 */

@Controller
@RequestMapping(value = "/users")
public class UserService {

    @Autowired
    private UserRepository repository;

    @GetMapping(value = "/findall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> findAll() {
        return new ResponseEntity<>((List<User>) repository.findAll(), HttpStatus.OK);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<User> create(@FormParam("name") String name, @FormParam("birthday") String birthday, @FormParam("emails") String emails) {
        JSONArray jsonArray = new JSONArray(emails);
        List<String> emailList = IntStream.range(0, jsonArray.length()).mapToObj(i -> jsonArray.getString(i)).collect(Collectors.toList());
        User user = new User(name, LocalDate.parse(birthday), emailList);
        repository.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}
