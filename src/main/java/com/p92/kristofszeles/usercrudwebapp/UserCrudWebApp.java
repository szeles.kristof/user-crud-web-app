package com.p92.kristofszeles.usercrudwebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserCrudWebApp {

    public static void main(String[] args) {
        SpringApplication.run(UserCrudWebApp.class, args);
    }
}
