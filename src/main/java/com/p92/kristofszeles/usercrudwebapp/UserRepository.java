package com.p92.kristofszeles.usercrudwebapp;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author szelesk
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    
}
