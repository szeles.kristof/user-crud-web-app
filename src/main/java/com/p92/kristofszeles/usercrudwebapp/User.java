package com.p92.kristofszeles.usercrudwebapp;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OrderColumn;
import org.apache.commons.validator.EmailValidator;

/**
 *
 * @author szelesk
 */
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private final String name;
    private final LocalDate birthday;
    @ElementCollection(fetch = FetchType.EAGER)
    @OrderColumn
    private final List<String> emails;

    public User() {
        this.name = null;
        this.birthday = null;
        this.emails = null;
    }

    public User(String name, LocalDate birthday, List<String> emails) {
        this.name = name;
        this.birthday = birthday;
        EmailValidator validator = EmailValidator.getInstance();
        for (int i = 0; i < emails.size(); ++i) {
            if (!validator.isValid(emails.get(i))) emails.remove(i);
        }
        this.emails = emails;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public List<String> getEmails() {
        return emails;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", birthday=" + birthday + ", emails=" + emails + '}';
    }
}
